function img_out = forward_mapping(img_in, T)
    % Creati o noua imagine prin aplicarea transformarii T pe imaginea
    % img_in (grayscale image), folosind forward mapping.
    %
    % Inputs
    % ------
    % img_in: imaginea care trebuie sa fie transformata.
    %      T: transformarea care trebuie aplicata asupra imaginii.
    %         matrice de 2x2.
    %
    % Outputs
    % -------
    % img_out: imaginea transformata (grayscale).

    tic ();

    [n m] = size (img_in);
    scale_x = n; %Vom mari spatiul de lucru de 4 ori
    scale_y = m;
    result = zeros (n , m , 2);
    for i=1:n
        for ji = 1:m
            aux = T*[i ; ji];
            x = round(aux(1)); %Algoritmul din enunt
            y = round(aux(2)); %Extragem indicii
            result (i , ji , 1) = x+scale_x;
            result (i , ji , 2) = y+scale_y;
        end
    end

    if min(min(result(:,:,1))) < 1
        result(:,:,1) += abs(min(min(result(:,:,1)))) +1;
    end
    if min(min(result(:,:,2))) < 1
        result(:,:,2) += abs(min(min(result(:,:,2)))) +1;
    end %Mutam imaginea in spatiul de lucru
    
    for i=1:n
        for ji=1:m
            final_raw(result(i,ji,1),result(i,ji,2)) = img_in (i,ji);
        end
    end  %Aplicam transformarea

    [n m] = size (final_raw);
    for i_x=1:n
        if sum(final_raw(i_x,:)) ~= 0
            break;
        end
    end % Cautam coltul stanga-sus
    for ji_y=1:m
        if sum(final_raw(:,ji_y)) != 0
            break;
        end
    end
    final_raw = final_raw (i_x:n,ji_y:m);
    %Pastram doar ce avem nevoie

    [n m] = size (final_raw);
    for i_x=1:n
         if sum(final_raw(i_x,:)) == 0
            break;
        end
    end %Cautam coltul dreapta jos
    for ji_y=1:m
        if sum(final_raw(:,ji_y)) == 0
            break;
        end
    end
    %Pastram doar ce avem nevoie
    img_out = final_raw (1:i_x -1,1:ji_y-1);
    imwrite(mat2gray(img_out),"test.png");

    toc()
endfunction