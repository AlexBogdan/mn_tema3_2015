%Am tratat mai intai cazurile mai simple : Cand se iese din
%dimensiunile matricei , cand row sau col sunt numere intregi
% In cauzl nefericit , am facut interpolare pe rows si dupa 
% aceea pe cols (cum se explica si in PDF)

function value = bilerp(img, row, col)
    % Folositi interpolare biliniara pentru a estima intensitatea imaginii
    % in pozitia row, col

    [n m] = size (img);
    if (row < 1 || row > n || col < 1 || col > m)
        value = 0;
        return;
    end

    row0 = floor (row);
    row1 = ceil (row);

    if (row0 == row1)
    	value = lerp (img(row , :) , col);
        return;
    end %Caz de lerp simplu

    col0 = floor (col);
    col1 = ceil (col);
    if (col0 == col1)
        value = lerp (img(: , col) , row);
        return;
    end %Caz de lerp simplu
    	
    aux1 = lerp (img(row0,:) , col);
    aux2 = lerp (img(row1,:) , col);

 	row = row - floor(row) + 1;
    value = lerp ([aux1 aux2] , row);

endfunction
