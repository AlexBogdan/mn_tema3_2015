function img_out = demo()
    %6 teste default cerute in enunt

    duck = imread("flapping_duck.png");
    bird = imread("flapping_bird.png");

    tic(); %Cronometram
    T = [-1 0 ; 0 -1];
    inverse_mapping(duck , T);
    rename "test.png" "duck_180.png";
    printf ("duck_180.png done ..\n");
    inverse_mapping(bird , T);
    rename "test.png" "bird_180.png";
    printf ("bird_180.png done ..\n");

    aux = sqrt(2)/2;
    T = [aux -aux; aux aux];
    inverse_mapping(duck , T);
    rename "test.png" "duck_45.png";
    printf ("duck_45.png done ..\n");
    inverse_mapping(bird , T);
    rename "test.png" "bird_45.png";
    printf ("bird_45.png done ..\n");

    T = [0.4 0 ; 0 0.4];
    inverse_mapping(duck , T);
    rename "test.png" "duck_scaled.png";
    printf ("duck_scaled.png done ..\n");
    inverse_mapping(bird , T);
    rename "test.png" "bird_scaled.png";
    printf ("bird_scaled.png done ..\n");
    toc()
end