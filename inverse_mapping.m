function img_out = inverse_mapping(img_in, T)
    % Creati o noua imagine prin aplicarea transformarii T pe imaginea
    % img_in (grayscale image), folosind inverse mapping si interpolare
    % biliniara
    %
    % Inputs
    % ------
    % img_in: imaginea care trebuie sa fie transformata.
    %      T: transformarea care trebuie aplicata asupra imaginii.
    %         matrice de 2x2.
    %
    % Outputs
    % -------
    % img_out: imaginea transformata (grayscale).

    %Calculam T^-1

    a = T(1,1);
    b = T(1,2);
    c = T(2,1); %Calculam inversa
    d = T(2,2);
    T_inv = 1/(a*d - b*c) *[d -b ; -c a];

    [n m] = size (img_in);
    n *= 1.5; m*= 1.5;

    for i=-n:n
        for ji = -m:m
            aux = T_inv*[i ; ji]; %Aplicam transformarea
            final_raw(i+n+1,ji+m+1) = bilerp(img_in , aux(1) , aux(2));
        end
    end

    [n m] = size (final_raw);
    for i_x=1:n
         if sum(final_raw(i_x,:)) ~= 0
            break;
        end
    end %Cautam coltul din stanga sus
    for ji_y=1:m
        if sum(final_raw(:,ji_y)) ~= 0
            break;
        end
    end
    %Pastram doar ce ne trebuie
    final_raw = final_raw (i_x:n,ji_y:m);

    [n m] = size (final_raw);
    for i_x=1:n
         if sum(final_raw(i_x,:)) == 0
            break;
        end
    end %Cautam coltul din dreapta-jos
    for ji_y=1:m
        if sum(final_raw(:,ji_y)) == 0
            break;
        end
    end
    %Pastram doar ce ne trebuie
    img_out = final_raw (1:i_x -1,1:ji_y-1);
    imwrite(mat2gray(img_out),"test.png");

endfunction
