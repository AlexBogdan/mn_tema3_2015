function value = trilerp(stack, row, col, level)
    % Folositi interpolarea triliniara pentru a estima intensitatea unei
    % imagini in pozitia row, col, level.

    [n m k] = size (stack);
    level0 = floor (level);
    level1 = ceil (level);
    if level < 1 || level > k
    	value = 0;
    	return;  
    end  %sanity check
    aux1 = bilerp (stack(: , : , level0) , row , col);
    aux2 = bilerp (stack(: , : , level1) , row , col); 
    %interpolare pe cele 2 planuri
    level = level - floor(level) + 1;
    value = lerp ([aux1 aux2] , level); %Interpolare intre cele 2 puncte
end