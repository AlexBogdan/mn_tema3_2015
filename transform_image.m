function img_out = transform_image(img_in, k)
    % Scalati imaginea cu un factor k prin aplicarea interpolarii
    % triliniare
    %
    % Inputs
    % ------
    % img_in: imaginea care trebuie transformata.
    %      k: factorul cu care trebuie scalata imaginea.
    %
    % Outputs
    % -------
    % img_out: imaginea transformata

    [n m] = size(img_in);
    stack = zeros (n , m , ceil(k));
    stack = image_stack (img_in , ceil(k));
    [n m l] = size (stack);
    for i=1:n
        for ji = 1:m
            img_out(i , ji) = trilerp (stack , i , ji , k);
        end
    end %Scoatem valorile de blur necesare
    imwrite(mat2gray(img_out),"test.png");
end
