-=-=-=-=-=-=-Tema 3 MN-=-=-=-=-=-=-=-=-=
-=-=-=Andrei Bogdan Alexandru=-=-=-=-=
-=-=-=-=-=-=314CA=-=-=-=

-=-=-=-=-Task 1-=-=-=-=-=

Lerp -> 
Extragem valoarea de la indicele cerut intr-un vector

Bilerp ->
Verificam daca suntem in cazul simplu in care avem 
linia sau coloana constanta , iar daca nu , obtinem punctele
de pe una dintre axe si facem interpolarea intre acestea.

Trilerp ->
Aplicam Bilerp pe cele 2 nivele si dupa lerp intre punctele obtinute.

Forward_Mapping ->
Dublam dimensiunea spatiului in care vom lucra pentru a avea loc sa
trasformam poza. Dupa ce aplicam transformarea , eliminam surplusul
de spatiu si obtinem imaginea finala.

Inverse_Mapping ->
Ca si la Forward_Mapping , dublam spatiul si aplicam algorimtul pentru 
a obtine noii indici si pentru matriccea finala , eliminam spatiul inutil
si scriem poza finala.

Demo ->
Formeaza cele 6 imaigni cerute in enunt , folosind matricele
[-1  0] Pentru 180 de grade
[ 0 -1]

[sqrt(2)/2 -sqrt(2)/2] Pentru 45 de grade
[sqrt(2)/2  sqrt(2)/2]

[0.4  0] Pentru o scalare de 0.4
[0  0.4] 

Image_Stack ->
Pornind de la matricea initiala si folosind matricea K propusa , obtinem 
numarul de nievle de blur de care vom avea nevoie prin functia conv2.

Transform_Iamge ->
Dupa ce obtinem Stack , vom aplica Trilerp pentru a obtine nivelul necesar
de blur pe are sa-l printam.