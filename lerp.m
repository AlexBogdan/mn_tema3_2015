function value = lerp(v, x)
    % Interpolare liniara unidimensionala
    %
    % Inputs
    % ------
    % v: un vector cu valorile functie
    % x: o noua pozitie in care sa se calculeze valoarea functiei

    x0 = floor(x);
    x1 = ceil(x);
    if (x < 1 || x > length(v))
    	value = 0; return;
    end
    if (x0 == x1)
    	value = v(x0); return;
    end
    
    value = v(x0) + (v(x1) - v(x0))*(x - x0)/(x1 - x0);
    %Am folosit formula de pe Wikipedia
endfunction
